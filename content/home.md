---
title: Home
date: 2018-01-18T00:24:02.000Z
image: asdf
---
# Strange Brew Homebrew Club

Strange Brew Homebrew Club usually meets the third Thursday of each
month. We are always looking for new all-grain brewers who are
interested in joining a club that could help them step up their game! (We are also happy to meet extract brewers who are thinking about jumping in to the world of all-grain brewing!)

Please email questions to <mailto:strangebrewhbc@gmail.com>.
