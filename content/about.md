---
title: "About"
date: 2018-01-18T16:27:18-08:00
draft: false
---

Strange Brew Homebrew Club is centered out of Oregon's Willamette
Valley with members from NW Oregon & SW Washington. Founded in 1994,
the club's mission is simply, "to help brewers make better beer!" Our
all grain brewers have taken many regional and national awards in
competitions across the country with some going pro. We have helped
brewers excel at the National Homebrew Competition, the Master
Championship of Amateur Brewers and many local competitions as well.
We also host the Northwest's largest homebrew annual competition, the
Slurp & Burp Open. With a small club, there are great opportunities
for interaction in all aspects of brewing. Our goal is to grow as a
club but maintain that small unique culture we love to help newcomers
make their best brew yet!

