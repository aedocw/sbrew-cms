Repo for strangebrew.org website.

Renew cert with "sudo certbot certonly -d strangebrew.org --manual"
^^ Not necessary, now cert is updated automatically

Site is built with Hugo, docs here: http://gohugo.io/documentation/

add a new page:
- hugo new blog/my-new-post.md
- edit my-new-post.md
- run "hugo server -D" to see what it looks like

Markdown reference: http://commonmark.org/help/
